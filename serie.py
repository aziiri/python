from bitarray import bitarray;
from time import time;
from math import sqrt;
t=time();
s=bitarray();
print('enter n such that the sieving is upto 10^n');
k=input();k=10**k;
for i in xrange(0,k):
    s.append(True)
l=len(s);
for i in xrange(2,l/2+1):
    if s[i]:
        for j in xrange(i,l/i+1):
            r=i*j;
            if r<l:
                s[r]=0;
                
s[0]=0;s[1]=0;
def f(n):
    r=int(sqrt(n));
    if r%2==0:
        return 1.;
    else :
        return -1.;
S=0;
for i in xrange(0,l):
    if s[i]:
        S=S+f(i)/i;
print('the sum over '+repr(sum(s))+' primes is : '+repr(S));
print('this process took '+repr(time()-t)+' seconds');


